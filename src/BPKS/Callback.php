<?php

namespace BPKS;

use PAMI\Client\Impl\ClientImpl;
use PAMI\Message\Action\OriginateAction;

class Callback
{

    /**
     * @var $amiClient ClientImpl
     */
    protected $amiClient;


    /**
     * @param \Closure $handler
     * @return $this
     */
    protected function _initEvents(\Closure $handler)
    {
        $this->amiClient->registerEventListener($handler);
        return $this;
    }


    /**
     * Callback constructor.
     * @param array $clientOptions
     */
    public function __construct(array $clientOptions)
    {
        $this->amiClient = new ClientImpl($clientOptions);

        $this->_initEvents(function () {
            //event processing
        });

        $this->amiClient->open();

        return $this;
    }

    /**
     * Callback destructor
     */
    public function __destruct()
    {
        $this->amiClient->close();
    }


    /**
     * @param $clientPhoneNumber
     * @param $localPhoneNumber
     * @return string JSON
     */
    public function send($clientPhoneNumber, $localPhoneNumber)
    {

        $originate = new OriginateAction('Local/' . $localPhoneNumber . '@from-office');

        $originate->setExtension($clientPhoneNumber);
        $originate->setCallerId($clientPhoneNumber);
        $originate->setContext('from-office');

        $originate->setAsync(false);
        $originate->setTimeout(15000);
        $originate->setPriority(1);

        $response = $this->amiClient->send($originate);

        return json_encode([
            'success' => $response->isSuccess(),
            'message' => $response->getMessage()
        ]);
    }
}